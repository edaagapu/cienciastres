from Arbol import *

nodo = Nodo("+")
nodo.setIzquierda(Nodo("93"))
nodo.setDerecha(Nodo("*",Nodo("8"),Nodo("7")))

print ("RECORRIDO INORDEN")
inorden = listaInorden(nodo)
print(inorden)

print ("RECORRIDO POSTORDEN")
postorden = listaPostorden(nodo)
print(postorden)

print ("RECORRIDO PREORDEN")
preorden = listaPreorden(nodo)
print(preorden)