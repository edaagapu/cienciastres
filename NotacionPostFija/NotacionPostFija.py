from Pila import *

def operacion(operador,numA,numB):
    if operador=="+":
        return numA+numB
    elif operador=="-":
        return numA-numB
    elif operador=="*":
        return numA*numB
    elif operador=="/":
        return numA/numB

archivo = open("expresiones.in")
expresiones = archivo.read()
archivo.close()

expresion = expresiones.split("\n")
informacion = []
for i in expresion:
    informacion.append(i.split(" "))

#Se quita la última línea.
informacion.pop()

pila = Pila()
resultados = []
for j in informacion:
    for k in range(0,len(j)):
        if j[k].isdigit():
            pila.apilar(float(j[k]))
        else:
            resultado = operacion(j[k],pila.desapilar(),pila.desapilar())
            pila.apilar(resultado)
    resultados.append(pila.desapilar())

archivo = open("expresiones.out","w")
for i in resultados:
    archivo.write(str(i))
    archivo.write("\n")