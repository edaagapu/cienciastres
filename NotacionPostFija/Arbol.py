class Nodo:
    def __init__(self,value,izquierda=None,derecha=None):
        self.valor = value
        self.izq = izquierda
        self.der = derecha
    
    def setDerecha(self,nodo):
        self.der = nodo
    
    def setIzquierda(self,nodo):
        self.izq = nodo
    
    def getDerecha(self):
        return self.der
    
    def getIzquierda(self):
        return self.izq
    
    def setValor(self,value):
        self.valor = value
    
    def getValor(self):
        return self.valor

def recorridoPreorden(nodo):
    if nodo!=None:
        return (str(nodo.getValor())+" "+recorridoPreorden(nodo.getIzquierda())+" "+recorridoPreorden(nodo.getDerecha()))
    else:
        return ""
def recorridoPostorden(nodo):
    if nodo!=None:
        return (recorridoPostorden(nodo.getIzquierda())+" "+recorridoPostorden(nodo.getDerecha())+" "+str(nodo.getValor()))
    else:
        return ""
def recorridoInorden(nodo):
    if nodo!=None:
        return (recorridoInorden(nodo.getIzquierda())+" "+str(nodo.getValor())+" "+recorridoInorden(nodo.getDerecha()))
    else:
        return ""

def listaInorden(nodo):
    resultado = recorridoInorden(nodo)
    resultado = resultado.split(" ")
    for i in range(len(resultado)-1,-1,-1):
        if len(resultado[i])==0:
            resultado.pop(i)
    return resultado

def listaPostorden(nodo):
    resultado = recorridoPostorden(nodo)
    resultado = resultado.split(" ")
    for i in range(len(resultado)-1,-1,-1):
        if len(resultado[i])==0:
            resultado.pop(i)
    return resultado

def listaPreorden(nodo):
    resultado = recorridoPreorden(nodo)
    resultado = resultado.split(" ")
    for i in range(len(resultado)-1,-1,-1):
        if len(resultado[i])==0:
            resultado.pop(i)
    return resultado