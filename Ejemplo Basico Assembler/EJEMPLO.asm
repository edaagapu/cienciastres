;probando saltos condicionales e incondicionales;
.model small
.stack
.data 
    msg4 db 10,3,7,'INGRESE EL NUMERO, SE COMPARA CON EL 3: ','$'
	msg5 db 10,3,7,' NUMERO INGRESADO: ','$'
    num1 db ?       ;origen;
    num2 db 3       ;destino;
    cont db 0
    cont1 db 0
    msg6 db 10,3,7,' SOY MAYOR, AHORA ME SUMO Y VALGO: ','$'
    msg1 db ' NUMEROS IGUALES','$'
    msg2 db ' NUMERO INGRESADO ES MAYOR','$'
    msg3 db ' NUMERO 3 ES MAYOR','$'
.code 
    Main:
    mov ax, @data
    
    mov ds, ax
    mov ah, 09h
 	lea dx, msg4
 	int 21h
 	mov ah, 01h
 	int 21h

 	sub al, 30h
 	mov num1, al
 		
 	mov ah, 09h
 	lea dx, msg5
 	int 21h

 	mov ah,02h
 	mov dl, num1
 	add dl,30h
 	int 21h 
    mov al, num1
    cmp al, num2
    
    ;uso de nemonico;
    jc mayor2;el numero 2 es mayor;
    jz igual ;son iguales los numeros;
    jnz mayor1;el numero 1 es mayor;           
    
.exit
igual:
    MOV AH, 09H
    LEA DX, MSG1
    INT 21H
    
    
                           
                           
.exit    
mayor1:
    MOV AH, 09H
    LEA DX, MSG2
    INT 21H
    ciclo:
        CMP cont1,5
        je salir
        MOV AH,09H
        LEA DX, msg6
        INT 21H
        INC cont
        INC cont1 
        mov al,num1
        mov bl,cont
        add al,bl
        mov cont,al    
        mov ah,02h
 	    mov dl, cont
 	    add dl,30h
 	    int 21h
 	JMP ciclo1
 	salir:
 	    .exit
        
                           
                           
.exit           

mayor2:
    MOV AH, 09H
    LEA DX, MSG3
    INT 21H  
    ciclo1:
        CMP cont1,5
        je salir1
        MOV AH,09H
        LEA DX, msg6
        INT 21H
        INC cont
        INC cont1 
        mov al,num2
        mov bl,cont
        add al,bl
        mov cont,al    
        mov ah,02h
 	    mov dl, cont
 	    add dl,30h
 	    int 21h
 	JMP ciclo1
 	salir1:
 	    .exit
                           
                           
.exit                       
END




