class Libro:
    def __init__(self,titulo,autor,tematica,paginas,editorial):
        self.autor = autor
        self.titulo = titulo
        self.tematica = tematica
        self.paginas = int(paginas)
        self.editorial = editorial

    def getTitulo(self):
        return self.titulo

    def getAutor(self):
        return self.autor

    def getTematica(self):
        return self.tematica

    def getPaginas(self):
        return self.paginas

    def getEditorial(self):
        return self.editorial

    def getInfo(self):
        return self.titulo+" | "+self.autor+" | "+self.tematica+" | "+str(self.paginas)+" | "+self.editorial
    
