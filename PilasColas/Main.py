from Libro import *
from Pila import *

archivo = []

for i in range(1,9):
    archivito = open("Datos/"+str(i)+".txt","r")
    informacion = archivito.read()
    informacion = informacion.split('\n')
    libro = Libro(informacion[0],informacion[1],informacion[2],informacion[3],informacion[4])
    archivo.append(libro)

#Organizacion por paginas
pila = Pila()
pilaD = Pila()

for i in range(0,len(archivo)):
    pila.apilar((archivo[i].getPaginas(),i))

pilaD.apilar(pila.desapilar())
while not pila.es_vacia():
    tempoU = pilaD.desapilar()
    tempoD = pila.desapilar()
    while tempoU[0]>tempoD[0] and not pilaD.es_vacia():
        pila.apilar(tempoU)
        tempoU = pilaD.desapilar()
    if pilaD.es_vacia():
        pilaD.apilar(tempoD)
        pilaD.apilar(tempoU)
    else:
        pilaD.apilar(tempoU)        
        pilaD.apilar(tempoD)        

print "ORGANIZADO POR No. DE PAGINAS"
while not pilaD.es_vacia():
    tempoU = pilaD.desapilar()
    print archivo[tempoU[1]].getInfo()

#Organizacion por Autores

pila = Pila()
pilaD = Pila()

for i in range(0,len(archivo)):
    pila.apilar((archivo[i].getEditorial(),i))

pilaD.apilar(pila.desapilar())
while not pila.es_vacia():
    tempoU = pilaD.desapilar()
    autorU = tempoU[0]
    tempoD = pila.desapilar()
    autorD = tempoD[0]
    while ord(autorU[0])<ord(autorD[0]) and not pilaD.es_vacia():
        pila.apilar(tempoU)
        tempoU = pilaD.desapilar()
    if pilaD.es_vacia():
        pilaD.apilar(tempoD)
        pilaD.apilar(tempoU)
    else:
        pilaD.apilar(tempoU)        
        pilaD.apilar(tempoD)        
        
print "ORGANIZADO POR EDITORIAL"
while not pilaD.es_vacia():
    tempoU = pilaD.desapilar()
    print archivo[tempoU[1]].getInfo()