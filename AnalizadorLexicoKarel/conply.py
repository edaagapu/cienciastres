import ply.lex as lex
tokens = [ 'GIRAR_IZQUIERDA','FUNCION','INICIAR','TERMINAR','APAGAR', 'ZUMBADOR' ]


t_ignore = ' #'
t_GIRAR_IZQUIERDA = r'girar-izquierda'
t_FUNCION = r'define-nueva-instruccion'
t_INICIAR = r'[iniciar-programa][inicia-ejecucion][inicio]'
t_TERMINAR = r'[finalizar-programa][termina_ejecucion][fin]'
t_APAGAR = r'apagate'
t_ZUMBADOR = r'[coge-zumbador][deja-zumbador]' 

def t_error(t):
    print("Caracter invalido '%s'" % t.value[0])
    t.lexer.skip(1)

lex.lex() # construye el lector

lex.input("")
while True:
    tok = lex.token()
    if not tok: break
    print (str(tok.value) + " - " + str(tok.type))
